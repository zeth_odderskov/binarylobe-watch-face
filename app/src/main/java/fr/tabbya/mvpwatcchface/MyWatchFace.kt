package fr.tabbya.mvpwatcchface

import android.annotation.SuppressLint
import android.content.*
import android.graphics.*
import android.graphics.drawable.ShapeDrawable
import android.graphics.drawable.shapes.RectShape
import android.os.*
import androidx.palette.graphics.Palette
import android.support.wearable.watchface.CanvasWatchFaceService
import android.support.wearable.watchface.WatchFaceService
import android.support.wearable.watchface.WatchFaceStyle
import android.util.Log
import android.view.SurfaceHolder
import fr.tabbya.mvpwatcchface.constants.Paints
import fr.tabbya.mvpwatcchface.constants.PrivateConstants
import fr.tabbya.mvpwatcchface.util.HandleRequest

import java.lang.ref.WeakReference
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.Calendar
import java.util.TimeZone


private const val INTERACTIVE_UPDATE_RATE_MS = 1000

private const val MSG_UPDATE_TIME = 0


class MyWatchFace : CanvasWatchFaceService() {

    override fun onCreateEngine(): Engine {
        return Engine()
    }

    private class EngineHandler(reference: MyWatchFace.Engine) : Handler() {
        private val mWeakReference: WeakReference<MyWatchFace.Engine> = WeakReference(reference)

        override fun handleMessage(msg: Message) {
            val engine = mWeakReference.get()
            if (engine != null) {
                when (msg.what) {
                    MSG_UPDATE_TIME -> engine.handleUpdateTimeMessage()
                }
            }
        }
    }

    inner class Engine : CanvasWatchFaceService.Engine() {

        private lateinit var mCalendar: Calendar

        private var mRegisteredTimeZoneReceiver = false
        private var mMuteMode: Boolean = false
        private var mCenterX: Float = 0F
        private var mCenterY: Float = 0F

        private var globalWidth: Float = 0F
        private var globalHeight: Float = 0F

        private var mSecondHandLength: Float = 0F
        private var sMinuteHandLength: Float = 0F
        private var sHourHandLength: Float = 0F

        private var isRequestRunning: Boolean = false

        /* Colors for all hands (hour, minute, seconds, ticks) based on photo loaded. */
        private var mWatchHandColor: Int = 0
        private var mWatchHandHighlightColor: Int = 0
        private var mWatchHandShadowColor: Int = 0

        private lateinit var mHourPaint: Paint
        private lateinit var mMinutePaint: Paint
        private lateinit var mSecondPaint: Paint
        private var postPaint: Paint = Paints.requestPaint

        private lateinit var mBackgroundPaint: Paint
        private lateinit var mBackgroundBitmap: Bitmap
        private lateinit var mGrayBackgroundBitmap: Bitmap
        private lateinit var settingsBitmap: Bitmap

        private var screenON: Boolean = true
        private var mLowBitAmbient: Boolean = false
        private var mBurnInProtection: Boolean = false
        private var isSettings: Boolean = false
        private var batteryLevel: Int = 0;

        /* Handler to update the time once a second in interactive mode. */
        private val mUpdateTimeHandler = EngineHandler(this)

        private val mTimeZoneReceiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                mCalendar.timeZone = TimeZone.getDefault()
                invalidate()
            }
        }

        override fun onCreate(holder: SurfaceHolder) {
            super.onCreate(holder)

            setWatchFaceStyle(
                WatchFaceStyle.Builder(this@MyWatchFace)
                    .setAcceptsTapEvents(true)
                    .build()
            )

            mCalendar = Calendar.getInstance()
            initializeBackground()

            registerReceiver(broadCastReceiver, IntentFilter(Intent.ACTION_BATTERY_CHANGED))
        }

        val broadCastReceiver = object : BroadcastReceiver() {
            override fun onReceive(contxt: Context?, intent: Intent?) {

                val level: Int = intent!!.getIntExtra(BatteryManager.EXTRA_LEVEL, -1)

                batteryLevel = level;

                // Log.d("TAG_BATTERY_LEVEL", level.toString());

            }
        }

        private fun initializeBackground() {
            mBackgroundPaint = Paint().apply {
                color = Color.BLACK
            }
            mBackgroundBitmap = BitmapFactory.decodeResource(resources, R.drawable.bg)
//            settingsBitmap = BitmapFactory.decodeResource(resources, R.drawable.settings)


            Palette.from(mBackgroundBitmap).generate {
                it?.let {
                    mWatchHandHighlightColor = it.getVibrantColor(Color.RED)
                    mWatchHandColor = it.getLightVibrantColor(Color.WHITE)
                    mWatchHandShadowColor = it.getDarkMutedColor(Color.BLACK)
                }
            }
        }


        override fun onDestroy() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME)

            unregisterReceiver(broadCastReceiver)
            super.onDestroy()
        }

        override fun onPropertiesChanged(properties: Bundle) {
            super.onPropertiesChanged(properties)
            mLowBitAmbient = properties.getBoolean(
                WatchFaceService.PROPERTY_LOW_BIT_AMBIENT, false
            )
            mBurnInProtection = properties.getBoolean(
                WatchFaceService.PROPERTY_BURN_IN_PROTECTION, false
            )
        }

        override fun onTimeTick() {
            super.onTimeTick()
            invalidate()
        }

        override fun onAmbientModeChanged(inAmbientMode: Boolean) {
            // super.onAmbientModeChanged(inAmbientMode)
            // screenON = !inAmbientMode
            screenON = true

            // Log.d("AMBIENT TAG", screenON.toString());

            if (screenON)
                postPaint = Paints.requestPaint
            else
                postPaint = Paints.requestPaintAmbient

            updateTimer()
        }

        override fun onInterruptionFilterChanged(interruptionFilter: Int) {
            super.onInterruptionFilterChanged(interruptionFilter)
            val inMuteMode = interruptionFilter == WatchFaceService.INTERRUPTION_FILTER_NONE

            if (mMuteMode != inMuteMode) {
                mMuteMode = inMuteMode
                mHourPaint.alpha = if (inMuteMode) 100 else 255
                mMinutePaint.alpha = if (inMuteMode) 100 else 255
                mSecondPaint.alpha = if (inMuteMode) 80 else 255
                invalidate()
            }
        }

        override fun onSurfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
            super.onSurfaceChanged(holder, format, width, height)

            mCenterX = width / 2f
            mCenterY = height / 2f

            globalWidth = width / 1f
            globalHeight = height / 1f

            mSecondHandLength = (mCenterX * 0.875).toFloat()
            sMinuteHandLength = (mCenterX * 0.75).toFloat()
            sHourHandLength = (mCenterX * 0.5).toFloat()


            val scale = width.toFloat() / mBackgroundBitmap.width.toFloat()

            mBackgroundBitmap = Bitmap.createScaledBitmap(
                mBackgroundBitmap,
                (mBackgroundBitmap.width * scale).toInt(),
                (mBackgroundBitmap.height * scale).toInt(), true
            )

            if (!mBurnInProtection && !mLowBitAmbient) {
                initGrayBackgroundBitmap()
            }
        }

        private fun initGrayBackgroundBitmap() {
            mGrayBackgroundBitmap = Bitmap.createBitmap(
                mBackgroundBitmap.width,
                mBackgroundBitmap.height,
                Bitmap.Config.ARGB_8888
            )
            val canvas = Canvas(mGrayBackgroundBitmap)
            val grayPaint = Paint()
            val colorMatrix = ColorMatrix()
            colorMatrix.setSaturation(0f)
            val filter = ColorMatrixColorFilter(colorMatrix)
            grayPaint.colorFilter = filter
            canvas.drawBitmap(mBackgroundBitmap, 0f, 0f, grayPaint)
        }

        override fun onTapCommand(tapType: Int, x: Int, y: Int, eventTime: Long) {
            when (tapType) {
                WatchFaceService.TAP_TYPE_TOUCH -> {
                    // The user has started touching the screen.
                }
                WatchFaceService.TAP_TYPE_TOUCH_CANCEL -> {
                    // The user has started a different gesture or otherwise cancelled the tap.
                }
                WatchFaceService.TAP_TYPE_TAP -> {

                }
                // The user has completed the tap gesture
            }
            invalidate()
        }


        override fun onDraw(canvas: Canvas, bounds: Rect) {
            val now = System.currentTimeMillis()
            mCalendar.timeInMillis = now

            drawBackground(canvas)

            if (!isSettings) {
                drawLines(canvas)
                drawDigitalDace(canvas)
                if(screenON)
                    drawRequest(canvas)
            } else {
                drawOnSettings(canvas)
            }

            if(screenON)
                drawSettings(canvas)

            if (!isRequestRunning) {
                isRequestRunning = !isRequestRunning

                HandleRequest(canvas, globalHeight, globalWidth, applicationContext).execute()

                val timer = object : CountDownTimer(45000, 1000) {
                    override fun onTick(millisUntilFinished: Long) {}

                    override fun onFinish() {

                        isRequestRunning = !isRequestRunning
                    }
                }
                timer.start()

            }
        }

        private fun drawSettings(canvas: Canvas) {

            // Battery level
            canvas.drawText(
                batteryLevel.toString() + "%",
                globalWidth / 2 + 45,
                60f,
                postPaint
            )
        }

        private fun drawOnSettings(canvas: Canvas) {

        }

        private fun drawRequest(canvas: Canvas) {
            val watchPaint: Paint = postPaint
            watchPaint.typeface = Typeface.create("Arial", Typeface.BOLD)
            // val topStartTOPLINE: Float = globalHeight / 3f - PrivateConstants.SPACEING_CONSTANT
            // val bottomStartTOPLINE: Float = globalHeight - globalHeight / 3f + PrivateConstants.WATCH_COLORED_LINE_HEIGHT + 2 * PrivateConstants.SPACEING_CONSTANT

            val firstLine: Float = 135f
            val secondLine: Float = 185f
            val thirdLine: Float = 235f
            val fourthLine: Float = 285f
            val fifthLine: Float = 335f

            val sharedPref: SharedPreferences = getSharedPreferences("MVPWATCH", 0)


            // First slot, first line
            var todaysCalories = sharedPref.getString("val1", "")!!
            var todaysCaloriesAsInt: Int
            if( ! todaysCalories.isEmpty() ){
                todaysCaloriesAsInt = todaysCalories.toInt()
            } else {
                todaysCaloriesAsInt = 0
            }


            val current = LocalDateTime.now()
            val currentHourNumber = current.getHour().toInt()






            if( currentHourNumber >= 12 && currentHourNumber < 20 ) {
                var targetCalories = ( ( currentHourNumber - 12 ) * 300 ) + 300
                var calorieDiff = targetCalories - todaysCaloriesAsInt

                if( calorieDiff <= -600 ){
                    todaysCalories = todaysCalories + "--"
                } else if( calorieDiff <= -300 ){
                    todaysCalories = todaysCalories + "-"
                } else if( calorieDiff > 600 ) {
                    todaysCalories = todaysCalories + "++"
                } else if( calorieDiff > 300 ) {
                    todaysCalories = todaysCalories + "+"
                }
            }



            // First line
            canvas.drawText(
                todaysCalories,
                2.5f * PrivateConstants.WATCH_PADDING_LINE_DISTANCE.toFloat(),
                firstLine,
                watchPaint
            )

            canvas.drawText(
                sharedPref.getString("val2", "")!!,
                globalWidth / 4 + 2.5f * PrivateConstants.WATCH_PADDING_LINE_DISTANCE.toFloat(),
                firstLine,
                watchPaint
            )
            canvas.drawText(
                sharedPref.getString("val3", "")!!,
                globalWidth / 2 + 2.5f * PrivateConstants.WATCH_PADDING_LINE_DISTANCE.toFloat(),
                firstLine,
                watchPaint
            )
            canvas.drawText(
                sharedPref.getString("val4", "")!!,
                3 * globalWidth / 4 + 2.5f * PrivateConstants.WATCH_PADDING_LINE_DISTANCE.toFloat(),
                firstLine,
                watchPaint
            )

            // Second line
            canvas.drawText(
                sharedPref.getString("val5", "")!!,
                2.5f * PrivateConstants.WATCH_PADDING_LINE_DISTANCE.toFloat(),
                secondLine,
                watchPaint
            )
            canvas.drawText(
                sharedPref.getString("val6", "")!!,
                globalWidth / 4 + 2.5f * PrivateConstants.WATCH_PADDING_LINE_DISTANCE.toFloat(),
                secondLine,
                watchPaint
            )
            canvas.drawText(
                sharedPref.getString("val7", "")!!,
                globalWidth / 2 + 2.5f * PrivateConstants.WATCH_PADDING_LINE_DISTANCE.toFloat(),
                secondLine,
                watchPaint
            )
            canvas.drawText(
                sharedPref.getString("val8", "")!!,
                3 * globalWidth / 4 + 2.5f * PrivateConstants.WATCH_PADDING_LINE_DISTANCE.toFloat(),
                secondLine,
                watchPaint
            )


            // Third line
            canvas.drawText(
                sharedPref.getString("val9", "")!!,
                2.5f * PrivateConstants.WATCH_PADDING_LINE_DISTANCE.toFloat(),
                thirdLine,
                watchPaint
            )
            canvas.drawText(
                sharedPref.getString("val10", "")!!,
                globalWidth / 4 + 2.5f * PrivateConstants.WATCH_PADDING_LINE_DISTANCE.toFloat(),
                thirdLine,
                watchPaint
            )
            canvas.drawText(
                sharedPref.getString("val11", "")!!,
                globalWidth / 2 + 2.5f * PrivateConstants.WATCH_PADDING_LINE_DISTANCE.toFloat(),
                thirdLine,
                watchPaint
            )
            canvas.drawText(
                sharedPref.getString("val12", "")!!,
                3 * globalWidth / 4 + 2.5f * PrivateConstants.WATCH_PADDING_LINE_DISTANCE.toFloat(),
                thirdLine,
                watchPaint
            )


            // Fourth line
            canvas.drawText(
                sharedPref.getString("val13", "")!!,
                2.5f * PrivateConstants.WATCH_PADDING_LINE_DISTANCE.toFloat(),
                fourthLine,
                watchPaint
            )
            canvas.drawText(
                sharedPref.getString("val14", "")!!,
                globalWidth / 4 + 2.5f * PrivateConstants.WATCH_PADDING_LINE_DISTANCE.toFloat(),
                fourthLine,
                watchPaint
            )
            canvas.drawText(
                    sharedPref.getString("val15", "")!!,
            globalWidth / 2 + 2.5f * PrivateConstants.WATCH_PADDING_LINE_DISTANCE.toFloat(),
            fourthLine,
            watchPaint
            )
            canvas.drawText(
                sharedPref.getString("val16", "")!!,
                3 * globalWidth / 4 + 2.5f * PrivateConstants.WATCH_PADDING_LINE_DISTANCE.toFloat(),
                fourthLine,
                watchPaint
            )



            // Bottom Meta
            canvas.drawText(
                sharedPref.getString("val17", "")!!,
                117f,
                fifthLine,
                watchPaint
            )

            canvas.drawText(
                sharedPref.getString("val18", "")!!,
                217f,
                fifthLine,
                watchPaint
            )



        }

        @SuppressLint("NewApi")
        private fun drawDigitalDace(canvas: Canvas) {
            val watchPaint: Paint
            if (screenON)
                watchPaint = Paints.watchPaint
            else
                watchPaint = Paints.watchPaintAmbient

            watchPaint.typeface = Typeface.create("Arial", Typeface.BOLD)





            // Time
            val current = LocalDateTime.now()
            // Log.d( "Hours", current.getHour().toString() )
            // val currentHourNumber = current.getHour().toInt();

            val formatter = DateTimeFormatter.ofPattern("HH:mm")
            val formatted = current.format(formatter)
            canvas.drawText(
                formatted,
                PrivateConstants.WATCH_PADDING_LINE_DISTANCE.toFloat() + 70,
                90f,
                watchPaint
            )





            val calendar = Calendar.getInstance()
            val day = calendar.get(Calendar.DAY_OF_WEEK)

            val dayOfTheWeek = getDayFromCalendar(day)

            val dayPaint: Paint
            if (screenON)
                dayPaint = Paints.dayPaint
            else
                dayPaint = Paints.dayPaintAmbient

            // Day
            canvas.drawText(
                dayOfTheWeek,
                globalWidth / 2 + 45,
                88f,
                dayPaint
            )

            val monthPaint: Paint
            if (screenON)
                monthPaint = Paints.monthPaint
            else
                monthPaint = Paints.monthPaintAmbient

            monthPaint.typeface = Typeface.create("Arial", Typeface.NORMAL)




            // Date
            val cal = Calendar.getInstance()
            val dayOfMonth = cal.get(Calendar.DAY_OF_MONTH)
            val monthValue = cal.get(Calendar.MONTH) + 1
            val combo: String = dayOfMonth.toString() + "." + monthValue

            canvas.drawText(
                combo,
                globalWidth / 2 + 73 + PrivateConstants.SPACEING_CONSTANT,
                88f,
                monthPaint
            )

        }

        private fun getDayFromCalendar(day: Int): String {
            when (day) {
                Calendar.SUNDAY -> {
                    return "sun"
                }
                Calendar.MONDAY -> {
                    return "mon"
                }
                Calendar.TUESDAY -> {
                    return "tue"
                }
                Calendar.WEDNESDAY -> {
                    return "wed"
                }
                Calendar.THURSDAY -> {
                    return "thu"
                }
                Calendar.FRIDAY -> {
                    return "fri"
                }
                Calendar.SATURDAY -> {
                    return "sat"
                }
            }

            return "mon";
        }

        private fun drawLines(canvas: Canvas) {

            // First line
            val shapeDrawableFirstLine = ShapeDrawable(RectShape())

            val leftStartFirstLine: Int = PrivateConstants.WATCH_PADDING_LINE_DISTANCE
            val topStartFirstLine: Int = 100
            val rightStartFirstLine: Int = globalWidth.toInt() - PrivateConstants.WATCH_PADDING_LINE_DISTANCE
            val bottomStartFirstLine: Int = ( topStartFirstLine + PrivateConstants.WATCH_COLORED_LINE_HEIGHT)

            shapeDrawableFirstLine.setBounds(leftStartFirstLine, topStartFirstLine, rightStartFirstLine, bottomStartFirstLine)
            if (screenON) {
                shapeDrawableFirstLine.paint.color = Color.parseColor(PrivateConstants.WATCH_LINE_COLLOR)
            } else {
                shapeDrawableFirstLine.paint.color = Color.parseColor(PrivateConstants.GRAY_COLOR)
            }
            shapeDrawableFirstLine.draw(canvas)





            // Second line
            val shapeDrawableSecondLine = ShapeDrawable(RectShape())

            val leftStartSecondLine: Int = PrivateConstants.WATCH_PADDING_LINE_DISTANCE
            val topStartSecondLine: Int = 150
            val rightStartSecondLine: Int = globalWidth.toInt() - PrivateConstants.WATCH_PADDING_LINE_DISTANCE
            val bottomStartSecondLine: Int = ( topStartSecondLine + PrivateConstants.WATCH_COLORED_LINE_HEIGHT)

            shapeDrawableSecondLine.setBounds(leftStartSecondLine, topStartSecondLine, rightStartSecondLine, bottomStartSecondLine)
            if (screenON) {
                shapeDrawableSecondLine.paint.color = Color.parseColor(PrivateConstants.WATCH_LINE_COLLOR)
            } else {
                shapeDrawableSecondLine.paint.color = Color.parseColor(PrivateConstants.GRAY_COLOR)
            }
            shapeDrawableSecondLine.draw(canvas)








            // Third line
            val shapeDrawableThirdLine = ShapeDrawable(RectShape())

            val leftStartThirdLine: Int = PrivateConstants.WATCH_PADDING_LINE_DISTANCE
            val topStartThirdLine: Int = 200
            val rightStartThirdLine: Int = globalWidth.toInt() - PrivateConstants.WATCH_PADDING_LINE_DISTANCE
            val bottomStartThirdLine: Int = ( topStartThirdLine + PrivateConstants.WATCH_COLORED_LINE_HEIGHT)

            shapeDrawableThirdLine.setBounds(leftStartThirdLine, topStartThirdLine, rightStartThirdLine, bottomStartThirdLine)
            if (screenON) {
                shapeDrawableThirdLine.paint.color = Color.parseColor(PrivateConstants.WATCH_LINE_COLLOR)
            } else {
                shapeDrawableThirdLine.paint.color = Color.parseColor(PrivateConstants.GRAY_COLOR)
            }
            shapeDrawableThirdLine.draw(canvas)









            // Fourth line
            val shapeDrawableFourthLine = ShapeDrawable(RectShape())

            val leftStartFourthLine: Int = PrivateConstants.WATCH_PADDING_LINE_DISTANCE
            val topStartFourthLine: Int = 250
            val rightStartFourthLine: Int = globalWidth.toInt() - PrivateConstants.WATCH_PADDING_LINE_DISTANCE
            val bottomStartFourthLine: Int = ( topStartFourthLine + PrivateConstants.WATCH_COLORED_LINE_HEIGHT)

            shapeDrawableFourthLine.setBounds(leftStartFourthLine, topStartFourthLine, rightStartFourthLine, bottomStartFourthLine)
            if (screenON) {
                shapeDrawableFourthLine.paint.color = Color.parseColor(PrivateConstants.WATCH_LINE_COLLOR)
            } else {
                shapeDrawableFourthLine.paint.color = Color.parseColor(PrivateConstants.GRAY_COLOR)
            }
            shapeDrawableFourthLine.draw(canvas)








            // Fifth line
            val shapeDrawableFifthLine = ShapeDrawable(RectShape())

            val leftStartFifthLine: Int = PrivateConstants.WATCH_PADDING_LINE_DISTANCE
            val topStartFifthLine: Int = 300
            val rightStartFifthLine: Int = globalWidth.toInt() - PrivateConstants.WATCH_PADDING_LINE_DISTANCE
            val bottomStartFifthLine: Int = ( topStartFifthLine + PrivateConstants.WATCH_COLORED_LINE_HEIGHT)

            shapeDrawableFifthLine.setBounds(leftStartFifthLine, topStartFifthLine, rightStartFifthLine, bottomStartFifthLine)
            if (screenON) {
                shapeDrawableFifthLine.paint.color = Color.parseColor(PrivateConstants.WATCH_LINE_COLLOR)
            } else {
                shapeDrawableFifthLine.paint.color = Color.parseColor(PrivateConstants.GRAY_COLOR)
            }
            shapeDrawableFifthLine.draw(canvas)
        }

        private fun drawBackground(canvas: Canvas) {
            canvas.drawColor(Color.BLACK)
        }

        override fun onVisibilityChanged(visible: Boolean) {
            super.onVisibilityChanged(visible)

            if (visible) {
                registerReceiver()
                /* Update time zone in case it changed while we weren't visible. */
                mCalendar.timeZone = TimeZone.getDefault()
                invalidate()
            } else {
                unregisterReceiver()
            }

            /* Check and trigger whether or not timer should be running (only in active mode). */
            updateTimer()
        }

        private fun registerReceiver() {
            if (mRegisteredTimeZoneReceiver) {
                return
            }
            mRegisteredTimeZoneReceiver = true
            val filter = IntentFilter(Intent.ACTION_TIMEZONE_CHANGED)
            this@MyWatchFace.registerReceiver(mTimeZoneReceiver, filter)
        }

        private fun unregisterReceiver() {
            if (!mRegisteredTimeZoneReceiver) {
                return
            }
            mRegisteredTimeZoneReceiver = false
            this@MyWatchFace.unregisterReceiver(mTimeZoneReceiver)
        }

        private fun updateTimer() {
            mUpdateTimeHandler.removeMessages(MSG_UPDATE_TIME)
            if (shouldTimerBeRunning()) {
                mUpdateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME)
            }
        }

        private fun shouldTimerBeRunning(): Boolean {
            return isVisible && !screenON
        }

        fun handleUpdateTimeMessage() {
            invalidate()
            if (shouldTimerBeRunning()) {
                val timeMs = System.currentTimeMillis()
                val delayMs = INTERACTIVE_UPDATE_RATE_MS - timeMs % INTERACTIVE_UPDATE_RATE_MS
                mUpdateTimeHandler.sendEmptyMessageDelayed(MSG_UPDATE_TIME, delayMs)
            }
        }
    }
}

