package fr.tabbya.mvpwatcchface.constants

class PrivateConstants {
    companion object {
        const val WATCH_FONT_CONSTANT: Float = 60f
        const val DAY_FONT_CONSTANT: Float = 20f
        const val REQUEST_FONT_CONSTANT: Float = 20f

        const val SPACEING_CONSTANT: Float = 10f

        //LINE CONSTANTS
        const val WATCH_COLORED_LINE_HEIGHT: Int = 5
        const val WATCH_PADDING_LINE_DISTANCE: Int = 7

        // const val WATCH_LINE_COLLOR = "#02E4A4"
        const val WATCH_LINE_COLLOR = "#168062"
        const val GRAY_COLOR = "#929292"

        //const val FONT_COLOR = "#FFFFFF"
        const val FONT_COLOR = "#f4f4f4"

        const val MINUTE_STROKE_WIDTH = 3f

    }
}
