package fr.tabbya.mvpwatcchface.constants

import android.graphics.Color
import android.graphics.Paint

class Paints {

    companion object {
        val watchPaint: Paint = Paint().apply {
            color =  Color.parseColor( PrivateConstants.FONT_COLOR )
            strokeWidth = PrivateConstants.MINUTE_STROKE_WIDTH
            isAntiAlias = true
            strokeCap = android.graphics.Paint.Cap.ROUND
            textSize= PrivateConstants.WATCH_FONT_CONSTANT
        }

        val watchPaintAmbient: Paint = Paint().apply {
            color =  Color.parseColor(PrivateConstants.GRAY_COLOR)
            strokeWidth = PrivateConstants.MINUTE_STROKE_WIDTH
            isAntiAlias = true
            strokeCap = android.graphics.Paint.Cap.ROUND
            textSize= PrivateConstants.WATCH_FONT_CONSTANT
        }

        val dayPaint: Paint = Paint().apply {
            color = Color.parseColor(PrivateConstants.WATCH_LINE_COLLOR)
            strokeWidth = PrivateConstants.MINUTE_STROKE_WIDTH
            isAntiAlias = true
            strokeCap = android.graphics.Paint.Cap.ROUND
            textSize= PrivateConstants.DAY_FONT_CONSTANT
        }

        val dayPaintAmbient: Paint = Paint().apply {
            color =  Color.parseColor(PrivateConstants.GRAY_COLOR)
            strokeWidth = PrivateConstants.MINUTE_STROKE_WIDTH
            isAntiAlias = true
            strokeCap = android.graphics.Paint.Cap.ROUND
            textSize= PrivateConstants.DAY_FONT_CONSTANT
        }

        val monthPaint: Paint = Paint().apply {
            color =  Color.parseColor( PrivateConstants.FONT_COLOR )
            strokeWidth = PrivateConstants.MINUTE_STROKE_WIDTH
            isAntiAlias = true
            strokeCap = android.graphics.Paint.Cap.ROUND
            textSize= PrivateConstants.DAY_FONT_CONSTANT
        }

        val monthPaintAmbient: Paint = Paint().apply {
            color =  Color.parseColor(PrivateConstants.GRAY_COLOR)
            strokeWidth = PrivateConstants.MINUTE_STROKE_WIDTH
            isAntiAlias = true
            strokeCap = android.graphics.Paint.Cap.ROUND
            textSize= PrivateConstants.DAY_FONT_CONSTANT
        }

        val requestPaint: Paint = Paint().apply {
            color =  Color.parseColor( PrivateConstants.FONT_COLOR )
            strokeWidth = PrivateConstants.MINUTE_STROKE_WIDTH
            isAntiAlias = true
            strokeCap = android.graphics.Paint.Cap.ROUND
            textSize= PrivateConstants.REQUEST_FONT_CONSTANT
        }

        val requestPaintAmbient: Paint = Paint().apply {
            color =  Color.parseColor(PrivateConstants.GRAY_COLOR)
            strokeWidth = PrivateConstants.MINUTE_STROKE_WIDTH
            isAntiAlias = true
            strokeCap = android.graphics.Paint.Cap.ROUND
            textSize= PrivateConstants.REQUEST_FONT_CONSTANT
        }
    }
}