package fr.tabbya.mvpwatcchface.util

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Canvas
import android.os.AsyncTask
import android.util.Log
import org.json.JSONObject
import java.io.BufferedInputStream
import java.io.BufferedReader
import java.io.InputStreamReader
import java.net.HttpURLConnection
import java.net.URL
import fr.tabbya.mvpwatcchface.constants.Paints
import fr.tabbya.mvpwatcchface.constants.PrivateConstants

class HandleRequest(canvas: Canvas, globalHeight: Float, globalWidth: Float, context: Context) : AsyncTask<Unit, Unit, String>() {

    private val localCanvas = canvas
    private val localGlobalHeight = globalHeight
    private val localGlobalWidth = globalWidth
    private val localContext: Context = context;

    override fun doInBackground(vararg params: Unit?): String?
    {

        try{
            val url = URL("https://binarylobe.zeth.dk/api/1/watch-face-right")
            val httpClient = url.openConnection() as HttpURLConnection
            // Log.d( "TESTHTTP", httpClient.responseCode.toString() )
            // Log.d( "TESTHTTP2", HttpURLConnection.HTTP_OK.toString() )
            if (httpClient.responseCode == HttpURLConnection.HTTP_OK)
            {
                try
                {
                    val stream = BufferedInputStream(httpClient.inputStream)
                    val data: String = readStream(inputStream = stream)
                    return data
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    httpClient.disconnect()
                }
            } else {
                println("ERROR ${httpClient.responseCode}")
            }
        // }catch(e:java.net.ConnectException){
        }catch(ex: Exception){
            Log.d( "Error! ", "dammit!")
        }

        return null
    }

    fun readStream(inputStream: BufferedInputStream): String {
        val bufferedReader = BufferedReader(InputStreamReader(inputStream))
        val stringBuilder = StringBuilder()
        bufferedReader.forEachLine { stringBuilder.append(it) }
        return stringBuilder.toString()
    }

    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)
        if( result != null ){
            val dayPaint = Paints.dayPaint
            val answer = JSONObject(result)
            // Log.d("TAG", answer.toString());

            localCanvas.drawText("12345", localGlobalWidth/2+localGlobalWidth/4+PrivateConstants.SPACEING_CONSTANT, localGlobalHeight/2f+PrivateConstants.DAY_FONT_CONSTANT/3f, dayPaint)
            val sharedPref: SharedPreferences = localContext.getSharedPreferences("MVPWATCH", 0)

            val editor = sharedPref.edit()

            editor.putString("val1", answer.getString("1"))
            editor.putString("val2", answer.getString("2"))
            editor.putString("val3", answer.getString("3"))
            editor.putString("val4", answer.getString("4"))
            editor.putString("val5", answer.getString("5"))
            editor.putString("val6", answer.getString("6"))
            editor.putString("val7", answer.getString("7"))
            editor.putString("val8", answer.getString("8"))
            editor.putString("val9", answer.getString("9"))
            editor.putString("val10", answer.getString("10"))
            editor.putString("val11", answer.getString("11"))
            editor.putString("val12", answer.getString("12"))
            editor.putString("val13", answer.getString("13"))
            editor.putString("val14", answer.getString("14"))
            editor.putString("val15", answer.getString("15"))
            editor.putString("val16", answer.getString("16"))
            editor.putString("val17", answer.getString("17"))
            editor.putString("val18", answer.getString("18"))
            editor.apply()
        }

    }

}